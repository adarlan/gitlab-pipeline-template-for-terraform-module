variable "foo" {
  type = string
}

variable "bar" {
  type = list(string)
}

variable "baz" {
  type = map(string)
}

output "foo" {
  value = var.foo
}

output "bar" {
  value = join(",", var.bar)
}
