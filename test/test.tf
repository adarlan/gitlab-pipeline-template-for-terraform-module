module "test" {
  source = "./.."
  foo    = "abc123"
  bar    = ["abc123", "def456"]
  baz    = { "abc" : "abc123", "def" : "def456" }
}
