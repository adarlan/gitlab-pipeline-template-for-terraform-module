# GitLab Pipeline Template for Terraform

## Configuration

```yml
include:
  - project: adarlan/gitlab-pipeline-template-for-terraform-module
    ref: v1
    file: template.gitlab-ci.yml
```

## Secrets

SECRET_TF (file)
